(function ($) {

    var handleRegister = function(e){
        e.preventDefault();
        var $form = $(this);
        var nonce = $(this).data('nonce');
        var data = $(this).serialize();
        var base = $(this).data('base');
        var url = base + '/wp-json/sb/custom/register?_wpnonce=' + nonce;
        $.ajax({
            url: url,
            data: data,
            method: 'post'
        })
            .done(function (response) {
                if(response.success){
                    if(response.url){
                        window.location.replace(response.url);
                    }else{
                        $form.hide();
                        $('#success-register').show();
                        $('#success-register').html('<p>' + response.message + '</p>');

                    }
                }else{
                    $('#errors').html('');
                    $('#errors').html('<p>' + response.message[0] + '</p>');
                }
            });


    };

    var addRadioCheck = function()
    {
        var parent = $(this).parent();
        $('.custom-parent').css('outline', 'none');
        parent.css('outline', '1px solid red');
    };

    $(document).on('ready', function () {
        $('#custom_register_form').on('submit', handleRegister);
        $('.custom-radio-check').on('change', addRadioCheck);
    });
}(jQuery));